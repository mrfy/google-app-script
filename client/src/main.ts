import 'bootstrap';
import './scss/app.scss';

import App from './App.vue';
import { createApp } from 'vue';
import router from './pages/router';

const app = createApp(App);

app.use(router);
app.mount('#app');
