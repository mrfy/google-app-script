import { createRouter, createWebHistory } from 'vue-router';

import ApprovedRequests from './ApprovedRequests.vue';
import NewRequest from './NewRequest.vue';
import RequestDetails from './RequestDetails.vue';
import Requests from './Requests.vue';

// const Requests = { template: '<div>Home</div>' };
// const NewRequest = { template: '<div>About</div>' };
// const RequestDetails = { template: '<div>RequestDetails</div>' };
// const ApprovedRequests = { template: '<div> AAAAA ApprovedRequests</div>' };

const routes = [
  { name: 'home', path: '/', component: Requests },
  { path: '/new', component: NewRequest },
  {
    name: 'detail',
    path: '/detail/:id',
    component: RequestDetails,
    props: true,
  },
  { name: 'approved', path: '/approved', component: ApprovedRequests },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;
